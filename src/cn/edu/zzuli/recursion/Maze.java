package cn.edu.zzuli.recursion;

public class Maze {

    public static void main(String[] args) {
        //创建一个二维数组用来模拟迷宫的地图。
        int [][] map = new int[8][7];
        //使用1表示墙
        //上下置为1
        for (int i = 0; i < 7; i++) {
            map[0][i] = 1;
            map[7][i] = 1;
        }

        //左右置为1
        for (int i = 0; i < 7; i++) {
            map[i][0] = 1;
            map[i][6] = 1;
        }

        //在第四行做一下挡板
        for (int i = 1; i < 3; i++) {
            map[3][i] = 1;
        }

        map[1][2] = 1;
        map[2][2] = 1;


        //打印一下地图
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(map[i][j] + "  ");
            }
            System.out.println();
        }
        setWay(map, 1, 1);

        System.out.println("寻路后的地图：");
        //打印一下地图
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(map[i][j] + "  ");
            }
            System.out.println();
        }
    }

    /**
     * 小球找路，规定小球从 (1,1) 开始出发 (6,5)为终点
     * 1表示墙，2表示可以通过，3表示这条路走不通, 0表示还没走过。
     * 确定一条走的策略，下->右->上->左,如果该点走不通就再回溯。
     * @param map 表示地图
     * @param i 从第几行找
     * @param j 从第几列找
     * @return 找到路就返回true，否则返回false
     */
    public static boolean setWay(int[][] map, int i, int j) {
        //map[6][5]代表已经找到通路
        if (map[6][5] == 2) {
            return true;
        } else {
            //判断当前点的情况。如果为0
            if (map[i][j] == 0) {
                map[i][j] = 2;
                if (setWay(map, i + 1,j)) {
                    //向下走，如果能走通
                    return true;
                }else if (setWay(map, i, j + 1)){
                    //向右走
                    return true;
                }else if (setWay(map, i -1 , j)){
                    //向上走
                    return true;
                }else if (setWay(map, i, j - 1)){
                    //向左走
                    return true;
                }else {
                    //如果都走不通的话，变为3
                    map[i][j] = 3;
                    return false;
                }
            }else {
                //map[i][j]是其他数字的话，都返回false,不需要我们再走一边。
                return false;
            }
        }

    }
}
