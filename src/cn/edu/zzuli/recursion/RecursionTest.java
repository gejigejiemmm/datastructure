package cn.edu.zzuli.recursion;

public class RecursionTest {

    public static void main(String[] args) {
        //递归简单来讲，就是自己调用自己
        test(4);//先输出2，再然后3，再然后4

        System.out.println(factorial(3)); // 1 * 2 * 3 = 6
    }

    public static void test(int n) {
        //递归必须有一个出口。
        if (n > 2) {
            //再次调用自己。并且 n -1,然后该方法进入阻塞状态。
            //会再次在栈中开辟一个新空间。该段代码，debug一下自然会了解
            test(n - 1);
        }
        System.out.println("n=" + n);
    }

    //阶乘问题
    public static int factorial(int n) {
        if (n == 1) {
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }
}
