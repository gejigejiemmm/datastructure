package cn.edu.zzuli.queue;

import java.util.Scanner;

public class CircleArrayQueueDemo {
    public static void main(String[] args) {
        CircleArrayQueue queue = new CircleArrayQueue(4);
		char key = ' '; 
		Scanner scanner = new Scanner(System.in);//
        boolean loop = true;
        
		while(loop) {
			System.out.println("s(show): 遍历队列");
			System.out.println("e(exit): 退出程序");
			System.out.println("a(add): 添加一个数据到队列");
			System.out.println("g(get): 从队列中取出一个数据");
			System.out.println("h(head): 显示队头元素");
			key = scanner.next().charAt(0);
			switch (key) {
			case 's':
				queue.showQueue();
				break;
			case 'a':
				System.out.println("输入一个值");
				int value = scanner.nextInt();
				queue.addQueue(value);
				break;
			case 'g': 
				try {
					int res = queue.getQueue();
					System.out.printf("取出的元素为%d\n", res);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 'h': 
				try {
					int res = queue.headQueue();
					System.out.printf("队首的元素为%d\n", res);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 'e': 
				scanner.close();
				loop = false;
				break;
			default:
				break;
            }
            
        }
        
        System.out.println("退出程序");
    }
}

//使用数组模拟队列
class CircleArrayQueue {
    //队列的最大容量
    private int maxSize;
    //队首
    private int front;
    //队尾
    private int rear;
    //存放的数据
    private int[] data;

    //创建一个队列
    public CircleArrayQueue(int maxSize) {
        this.maxSize = maxSize;
        data = new int[maxSize];
        front = 0;//指向队列中第一个元素
        rear = 0;//指向队列中最后一个元素的后一个位置
    }

    //添加数据到队列
    public void addQueue(int n) {
        if (isFull()) {
            System.out.println("队列已满");
            return;
        }

        //先添加元素
        data[rear] = n;

        //再让队尾指针后移
        //但是要注意是环形队列，所以我们可以对 maxSize求余
        rear = (rear + 1) % maxSize;
        
    }

    //获取队列元素（出队列）
    public int getQueue() {
        if (isEmpty()) {
            throw new RuntimeException("队列为空,不能取数据");
        }

        //这里一定要注意啊，因为我们的 队首指针也是会环形变化的
        //所以我们可以先把值拿出来，然后再对指针进行操作
        //队首指针后移
        int value = data[front];
        front = (front + 1) % maxSize;
        return value;
    }

    //遍历队列的元素
    public void showQueue() {
        if (isEmpty()) {
            System.out.println("队列为空，没有数据");
            return;
        }
        //这里我们可以直接遍历 有效元素个数。
        //而且还有一点，在环形队列中 i 也是有可能大于 最大下标的
        for (int i = front; i < front + size(); i++) {
            System.out.println(data[(i % maxSize)]);
        }
    }

    //判断队列中的有效元素个数
    public int size() {
        return (rear + maxSize - front) % maxSize;
    }

    //显示队列的头数据
    public int headQueue() {
        if(isEmpty()) {
            throw new RuntimeException("队列为空,不能取数据");
        }

        return data[front];
    }

    //判断队列是否满了
    public boolean isFull() {
        return (rear + 1) % maxSize == front;
    }

    public boolean isEmpty() {
        //队首==队尾时 说明队列为空
        return front == rear;
    }

}