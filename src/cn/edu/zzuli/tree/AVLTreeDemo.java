package cn.edu.zzuli.tree;

public class AVLTreeDemo {

    public static void main(String[] args) {

        //左旋转
        //int[] arr = {4, 3, 6, 5, 7, 8};
        //右旋转
        //int[] arr = {10, 12, 8, 9, 7, 6};
        //这组数据会发现，单旋转并不能让我们的树变为平衡二叉树
        int[] arr = {10, 11, 7, 6, 8, 9};
        AVLTree avlTree = new AVLTree();
        for (int i = 0; i < arr.length; i++) {
            avlTree.add(new AVLNode(arr[i]));
        }
        avlTree.midOrder(avlTree.root);
        System.out.println("当前根节点为" + avlTree.root);
        System.out.println("树的高度" + avlTree.root.height());
        System.out.println("左子树的高度" + avlTree.root.leftHeight());
        System.out.println("右子树的高度" + avlTree.root.rightHeight());
    }

}

class AVLTree {
    AVLNode root;

    /**
     * 因为右子树高度 - 左子树高度 > 1
     * 左旋转 变为平衡二叉树
     *
     * @param node 在非双旋（左右都要旋转）的情况下，为root
     */
    public void leftRotate(AVLNode node) {
        //首先创建一个新节点，值为当前节点的值
        AVLNode newNode = new AVLNode(node.id);
        //新节点left 指向当前节点的left
        newNode.left = node.left;
        //新节点的right 指向当前节点的右子树的左子树
        newNode.right = node.right.left;
        //当前节点的值替换成右子树的值
        node.id = node.right.id;
        //当前节点右子树设置成当前节点的右子树的右子树
        node.right = node.right.right;
        //当前节点的左子树设置成新的节点
        node.left = newNode;
    }

    /**
     * 左子树高度 - 右子树高度 > 1
     * 右旋转 变为平衡二叉树
     * @param node 在非双旋的情况下，为root
     */
    public void rightRotate(AVLNode node) {
        //首先创建一个新节点，值为当前节点的值
        AVLNode newNode = new AVLNode(node.id);
        //新节点right 指向当前节点的right
        newNode.right = node.right;
        //新节点left 指向当前节点的left的right
        newNode.left = node.left.right;
        //当前节点的值替换成左子树的值
        node.id = node.left.id;
        node.left = node.left.left;
        node.right = newNode;
    }

    //以下代码就是二叉排序数的代码,add方法要修改
    public void add(AVLNode node) {
        if (root == null) {
            root = node;
            return;
        }

        if (node == null) {
            return;
        }

        AVLNode curNode =  root;
        while (curNode != null) {
            if (curNode.id > node.id) {
                //说明curNode比新增节点大，新增节点应该在当前节点的左边
                if (curNode.left == null) {
                    //如果当前节点的左子树为null，那么我们可以直接加入到curNode左边。
                    curNode.left = node;
                    break;
                }
                curNode = curNode.left;
            }else {
                if (curNode.right == null) {
                    //如果当前节点的右子树为null，那么我们可以直接加入到curNode右边。
                    curNode.right = node;
                    break;
                }
                curNode = curNode.right;
            }
        }
        int leftHeight = root.leftHeight();
        int rightHeight = root.rightHeight();
        //判断是否需要左旋转
        if (rightHeight - leftHeight > 1) {
            System.out.println("左旋转");
            //如果右子树的左子树高度大于右子树的右子树高度的话，那么我们还要进行一次右旋转
            if (root.right != null && root.right.leftHeight() > root.right.rightHeight()) {
                //右节点
                System.out.println("右子树右旋转");
                leftRotate(root.right);
            }
            leftRotate(root);
            return;
        }

        //判断是否需要右旋转
        if (leftHeight - rightHeight > 1) {
            System.out.println("右旋转");
            //如果左子树的右子树高度大于左子树的左子树高度的话，那么我们还要进行一次左旋转
            if (root.left != null && root.left.rightHeight() > root.left.leftHeight()) {
                //左节点
                System.out.println("左子树左旋转");
                leftRotate(root.left);
            }
            rightRotate(root);
        }

    }

    //中序遍历
    public void midOrder(AVLNode node) {
        if (root == null) {
            return;
        }
        if (node.left != null) {
            midOrder(node.left);
        }
        System.out.println(node);
        if (node.right != null) {
            midOrder(node.right);
        }
    }
    /**
     * 查找要删除的节点
     *
     * @param value 希望删除的节点的值
     */
    public AVLNode search(int value) {
        return this.search(root, value);
    }

    public AVLNode search(AVLNode node,int value) {
        if (node.id == value) {
            return node;
        } else if (value < node.id) {
            if (node.left != null) {
                return search(node.left, value);
            }
            return null;
        } else {
            if (node.right != null) {
                return search(node.right, value);
            }
            return null;
        }
    }

    //查找要删除节点的父节点
    public AVLNode searchParent(AVLNode node, int value) {
        //左子树不为null，并且左子树值相等 or（右子树不为null，右子树值相等）
        if ((node.left != null && node.left.id == value) ||
                (node.right != null && node.right.id == value)){
            return node;
        }else {
            //比当前节点值小，去递归左子树找，否则去右子树
            if (value < node.id && node.left != null) {
                return searchParent(node.left, value);
            }else if (value > node.id && node.right != null) {
                return searchParent(node.right, value);
            }
            //都不满足返回null
            return null;
        }
    }

    public void delNode(int value) {
        if (root != null) {
            //当前树只有根节点的情况，我们可以直接置空
            if (root.left == null && root.right ==null) {
                root = null;
                return;
            }
            AVLNode targetNode = search(value);
            //没找到要和删除的节点
            if (targetNode == null) {
                return;
            }
            //找到要删除节点的父节点
            AVLNode parentNode = searchParent(root,value);

            //如果当前节点为叶子节点
            if (targetNode.left == null && targetNode.right == null){
                //判断当前节点是父节点左子树还是右子树
                if (parentNode.left != null && value == parentNode.left.id) {
                    parentNode.left = null;
                }else if (parentNode.right != null && value == parentNode.right.id){
                    parentNode.right = null;
                }
            }else if (targetNode.left != null && targetNode.right != null) {
                //当前节点有两个子节点,即有两个左右子树
                int min = delRightTreeMin(targetNode.right);
                targetNode.id = min;

            }else {
                //当前节点有一个子节点
                //如果要删除的节点有左子树
                if (targetNode.left != null) {
                    //如果父节点为null，那么该节点为根节点
                    if (parentNode != null) {
                        //并且当前节点 为父节点的左子树
                        if (value == parentNode.left.id) {
                            parentNode.left = targetNode.left;
                        }else {
                            //当前节点为父节点的右子节点
                            parentNode.right = targetNode.left;
                        }
                    }else {
                        root = targetNode.left;
                    }
                }else {
                    if (parentNode != null) {
                        //要删除的节点有右子节点
                        //并且当前节点 为父节点的左子树
                        if (value == parentNode.left.id) {
                            parentNode.left = targetNode.right;
                        }else {
                            //当前节点为父节点的右子节点
                            parentNode.right = targetNode.right;
                        }
                    }else {
                        root = targetNode.right;
                    }
                }

            }


        }
    }

    /**
     * 查找右子树中的最小节点
     * @param node
     * @return
     */
    public int delRightTreeMin(AVLNode node) {
        AVLNode target = node;
        //循环的查找左子节点,就会找到最小值
        while (target.left != null) {
            target = target.left;
        }
        //这时target已经指向了最小节点,我们需要把这个节点给删掉
        delNode(target.id);
        return target.id;
    }

}

class AVLNode {
    int id;
    AVLNode left;
    AVLNode right;

    @Override
    public String toString() {
        return "AVLNode{" +
                "id=" + id +
                '}';
    }

    public AVLNode(int id) {
        this.id = id;
    }

    //返回以当前节点为根节点的树的高度(当前节点+子树有多高)
    public int height() {
        return Math.max(left == null ? 0 : left.height(),right == null ? 0 : right.height()) + 1;
    }

    //返回左子树的高度
    public int leftHeight() {
        if (left == null) {
            return 0;
        }
        return left.height();
    }

    public int rightHeight() {
        if (right == null) {
            return 0;
        }
        return right.height();
    }

}
