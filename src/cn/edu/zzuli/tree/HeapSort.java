package cn.edu.zzuli.tree;

import java.util.Arrays;

public class HeapSort {

    //堆排序
    public static void main(String[] args) {
        //要求将数组进行升序排序
        int[] arr = {4, 6, 8, 5, 9};
        heapSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    //编写一个堆排序的方法
    public static void heapSort(int[] arr) {
        //初步生成大顶堆
        for (int i = arr.length / 2 - 1; i >= 0 ; i--) {
            adjustHeap(arr, i, arr.length);
        }
//        System.out.println(Arrays.toString(arr));
        //进行排序调整，最大的值放到数组的末尾(堆顶元素与末尾元素交换)
        int temp = 0;
        for (int i = arr.length - 1; i >0 ; i--) {
            temp = arr[i];
            arr[i] = arr[0];
            arr[0] = temp;
            //交换后顺序发送变化，重新生成大顶堆
            adjustHeap(arr, 0, i);
        }

    }

    //将一个数组（顺序二叉树），调整成一个大顶堆
    public static void adjustHeap(int[] arr, int i, int length) {
        int temp = arr[i];

        //k = i * 2 +1,k 是 i节点的左子节点
        for (int k = 2 * i + 1; k < length; k = k * 2 + 1) {
            //左子节点比右子节点小的时候
            if (k + 1 < length && arr[k] < arr[k + 1]) {
                k++;
            }
            //如果子节点大于父节点
            if (arr[k] > temp) {
                //交换位置
                arr[i] = arr[k];
                //i指向k，继续循环比较
                i = k;
            }else {
                break;
            }
        }

        //for循环结束后，限制i 已经为最大值的原来下标，调整位置
        arr[i] = temp;
    }

}
