package cn.edu.zzuli.tree;

public class BinaryTreeDemo {

    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        Node root = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        root.left = node2;
        root.right = node3;
        node3.left = node5;
        node3.right = node4;

        binaryTree.setRoot(root);
//        binaryTree.preOrder(root);
//        binaryTree.midOrder(root);
        binaryTree.postOrder(root);
    }

}

class BinaryTree {
    //根节点
    private Node root;

    public void setRoot(Node root) {
        this.root = root;
    }

    //前序遍历
    public void preOrder(Node root) {
        //首先输出父节点
        System.out.println(root);
        //判断左节点是否为空
        if (root.left != null) {
            preOrder(root.left);
        }
        if (root.right != null) {
            preOrder(root.right);
        }
    }
    //中序遍历
    public void midOrder(Node root) {
        //判断左节点是否为空
        if (root.left != null) {
            midOrder(root.left);
        }
        //输出父节点
        System.out.println(root);
        //判断右节点
        if (root.right != null) {
            midOrder(root.right);
        }
    }
    //后序遍历
    public void postOrder(Node root) {
        //判断左节点是否为空
        if (root.left != null) {
            postOrder(root.left);
        }
        //判断右节点
        if (root.right != null) {
            postOrder(root.right);
        }
        //输出父节点
        System.out.println(root);
    }
}
