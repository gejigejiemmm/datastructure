package cn.edu.zzuli.tree;

public class BinarySortTreeDemo {

    public static void main(String[] args) {

        int[] arr = {7, 3, 10, 12, 5, 1, 9, 2};

        BinarySortTree tree = new BinarySortTree();

        for (int i = 0; i < arr.length; i++) {
            Node node = new Node(arr[i]);
            tree.add(node);
        }

        tree.midOrder(tree.root);
        tree.delNode(2);
        tree.delNode(5);
        tree.delNode(9);
        tree.delNode(12);
        tree.delNode(7);
        tree.delNode(3);
        tree.delNode(10);
        tree.delNode(1);
        tree.midOrder(tree.root);

    }

}

class BinarySortTree {

    Node root;

    public void add(Node node) {
        if (root == null) {
            root = node;
        }else {
            if (node == null) {
                return;
            }
            Node curNode =  root;
            while (curNode != null) {
                if (curNode.id > node.id) {
                    //说明curNode比新增节点大，新增节点应该在当前节点的左边
                    if (curNode.left == null) {
                        //如果当前节点的左子树为null，那么我们可以直接加入到curNode左边。
                        curNode.left = node;
                        return;
                    }
                    curNode = curNode.left;
                }else {
                    if (curNode.right == null) {
                        //如果当前节点的右子树为null，那么我们可以直接加入到curNode右边。
                        curNode.right = node;
                        return;
                    }
                    curNode = curNode.right;
                }
            }

        }
    }

    //中序遍历
    public void midOrder(Node node) {
        if (root == null) {
            return;
        }
        if (node.left != null) {
            midOrder(node.left);
        }
        System.out.println(node);
        if (node.right != null) {
            midOrder(node.right);
        }
    }
    /**
     * 查找要删除的节点
     *
     * @param value 希望删除的节点的值
     */
    public Node search(int value) {
        return this.search(root, value);
    }

    public Node search(Node node,int value) {
        if (node.id == value) {
            return node;
        } else if (value < node.id) {
            if (node.left != null) {
                return search(node.left, value);
            }
            return null;
        } else {
            if (node.right != null) {
                return search(node.right, value);
            }
            return null;
        }
    }

    //查找要删除节点的父节点
    public Node searchParent(Node node, int value) {
        //左子树不为null，并且左子树值相等 or（右子树不为null，右子树值相等）
        if ((node.left != null && node.left.id == value) ||
                (node.right != null && node.right.id == value)){
            return node;
        }else {
            //比当前节点值小，去递归左子树找，否则去右子树
            if (value < node.id && node.left != null) {
                return searchParent(node.left, value);
            }else if (value > node.id && node.right != null) {
                return searchParent(node.right, value);
            }
            //都不满足返回null
            return null;
        }
    }

    public void delNode(int value) {
        if (root != null) {
            //当前树只有根节点的情况，我们可以直接置空
            if (root.left == null && root.right ==null) {
                root = null;
                return;
            }
            Node targetNode = search(value);
            //没找到要和删除的节点
            if (targetNode == null) {
                return;
            }
            //找到要删除节点的父节点
            Node parentNode = searchParent(root,value);

            //如果当前节点为叶子节点
            if (targetNode.left == null && targetNode.right == null){
                //判断当前节点是父节点左子树还是右子树
                if (parentNode.left != null && value == parentNode.left.id) {
                    parentNode.left = null;
                }else if (parentNode.right != null && value == parentNode.right.id){
                    parentNode.right = null;
                }
            }else if (targetNode.left != null && targetNode.right != null) {
                //当前节点有两个子节点,即有两个左右子树
                int min = delRightTreeMin(targetNode.right);
                targetNode.id = min;

            }else {
                //当前节点有一个子节点
                //如果要删除的节点有左子树
                if (targetNode.left != null) {
                    //如果父节点为null，那么该节点为根节点
                    if (parentNode != null) {
                        //并且当前节点 为父节点的左子树
                        if (value == parentNode.left.id) {
                            parentNode.left = targetNode.left;
                        }else {
                            //当前节点为父节点的右子节点
                            parentNode.right = targetNode.left;
                        }
                    }else {
                        root = targetNode.left;
                    }
                }else {
                    if (parentNode != null) {
                        //要删除的节点有右子节点
                        //并且当前节点 为父节点的左子树
                        if (value == parentNode.left.id) {
                            parentNode.left = targetNode.right;
                        }else {
                            //当前节点为父节点的右子节点
                            parentNode.right = targetNode.right;
                        }
                    }else {
                        root = targetNode.right;
                    }
                }

            }


        }
    }

    /**
     * 查找右子树中的最小节点
     * @param node
     * @return
     */
    public int delRightTreeMin(Node node) {
        Node target = node;
        //循环的查找左子节点,就会找到最小值
        while (target.left != null) {
            target = target.left;
        }
        //这时target已经指向了最小节点,我们需要把这个节点给删掉
        delNode(target.id);
        return target.id;
    }

}

