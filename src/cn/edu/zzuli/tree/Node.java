package cn.edu.zzuli.tree;

public class Node {
    int id;
    Node left;
    Node right;

    //为了在线索化二叉树中区分
    //0表示子节点，1表示前驱或后继结点
    int leftType;
    int rightType;

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                '}';
    }

    public Node(int id) {
        this.id = id;
    }
}
