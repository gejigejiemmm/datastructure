package cn.edu.zzuli.sort;

import java.util.Arrays;

public class MergeSort  {

    public static void main(String[] args) {
        //归并排序
        int[] arr = {8, 4, 5, 7, 1, 3, 6, 2};
        int[] temp = new int[arr.length];
        mergeSort(arr, 0, arr.length - 1, temp);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(temp));
    }

    //分解+合并
    public static void mergeSort(int[] arr, int left, int right, int[] temp) {
        //如果 left >= right，不符合我们的递归条件
        if (left < right) {
            //先获取 mid
            int mid = (left + right) / 2;
            //向左递归进行分解
            mergeSort(arr, left, mid, temp);
            //向右递归进行分解
            mergeSort(arr, mid + 1, right, temp);
            //左右分解后，我们可以开始合并了
            merge(arr, left, mid, right, temp);
        }
    }

    /**
     * 合并的代码，和合并有序数组和有序链表的思想一样。
     * @param arr   排序的原始数组
     * @param left  左边有序序列的索引
     * @param mid   中间索引(left + right)/ 2
     * @param right 右边有序序列的索引
     * @param temp  第三方做中转数组
     */
    public static void merge(int[] arr, int left,int mid, int right, int[] temp) {
        int i = left;   //左边序列的下标指针
        int j = mid + 1;//右边序列下标指针
        int t = 0;      //temp的下标指针

        //将左右两边的有序数据按照规则填充到temp数组
        while (i <= mid && j <= right) {
            //如果 左边的小，就放入到temp中，然后i++
            //两者都要进行t++
            if (arr[i] <= arr[j]) {
                temp[t++] = arr[i++];
            }else {
                //如果 右边的小，就放入到temp中，然后j++
                temp[t++] = arr[j++];
            }
        }

        //运行到这，进行过数组合并的同学肯定都知道最后总会有一方，剩余，接下来就将剩余部分全部填充到数组。
        //如果左边剩余
        while (i <= mid) {
            temp[t++] = arr[i++];
        }

        //如果右边剩余
        while (j <= right) {
            temp[t++] = arr[j++];
        }

        System.out.println(Arrays.toString(temp));
        //现在我们的temp中的数据已经是有序的了，那么我们还要将其赋值到arr中
        t = 0;
        int tempIndex = left;
        while (tempIndex <= right) {
            arr[tempIndex++] = temp[t++];
        }

    }

}
