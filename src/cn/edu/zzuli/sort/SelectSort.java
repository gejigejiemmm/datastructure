package cn.edu.zzuli.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectSort {

    public static void main(String[] args) {
        //选择排序的例子[101,34,119,1]，时间复杂度也是 o(n^2)
        int[] arr = {101, 34, 119, 1};

        for (int i = 0; i < arr.length - 1; i++) {
            //假定 i 为最小值。
            int minindex = i;
            int min = arr[i];

            for (int j = i + 1; j < arr.length; j++) {
                //如果我们假定的最小值并非最小的话，记录值和下标
                if (min > arr[j]){
                    min = arr[j];
                    minindex = j;
                }
            }

            //现在 minidex 已经是最小的了
            if (minindex != i) {
                arr[minindex] = arr[i];
                //min 就是最小的值
                arr[i] = min;
            }

            System.out.println("第"+(i + 1) + "轮排序后~：" + Arrays.toString(arr));

        }
    }

}
