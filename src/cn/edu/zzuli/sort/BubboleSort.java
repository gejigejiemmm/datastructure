package cn.edu.zzuli.sort;

import java.util.Arrays;

//冒泡排序
public class BubboleSort {

    public void hello() {

    }

    public void say() {
        hello();
    }

    public static void main(String[] args) {
        int arr[] = {3, 9, -1, 10, -5};


        //临时变量
        int temp = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            //标识本次循环是否有交换，如果没有交换，说明已经排好序了，我们没必要往下继续执行，可以直接退出
            boolean flag = false;
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    flag = true;
                    temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
            System.out.println("第"+ (i + 1) +"趟排序: "+ Arrays.toString(arr));
            if (!flag) {
                break;
            }
        }
    }

}
