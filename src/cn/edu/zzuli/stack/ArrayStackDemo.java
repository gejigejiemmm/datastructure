package cn.edu.zzuli.stack;

public class ArrayStackDemo {

    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack(4);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        stack.list();

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.list();

    }

}

class ArrayStack {
    private int maxSize;
    private int[] stack;
    private int top = -1;

    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[maxSize];
    }

    //判断栈是否为null
    //判断栈是否满了
    public boolean isFull() {
        return this.top == maxSize - 1;
    }

    public boolean isEmpety() {
        return this.top == -1;
    }

    //入栈
    public void push(int data) {
        if (isFull()) {
            System.out.println("栈满");
            return;
        }
        top++;
        stack[top] = data;
    }

    //栈顶元素出栈
    public int pop() {
        if (isEmpety()) {
            throw new RuntimeException("栈中没有元素");
        }
        int value = stack[top];
        top--;
        return value;
    }

    public void list() {
        if (isEmpety()) {
            System.out.println("栈中没有元素");
            return;
        }

        for (int i = top; i >= 0; i--) {
            System.out.println(stack[i]);
        }
    }
}