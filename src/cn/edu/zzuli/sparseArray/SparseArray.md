## 稀疏数组
什么叫稀疏数组？
如果二维数组中的大部分元素为0，或者为同一个值的数组时，可以使用稀疏数组来保存该数组。
相比与 二维数组而言，稀疏数组存放的数据更少，如果通过稀疏数组的方式存入信息，会占用更小的磁盘空间
而且，就如五子棋而言，我们只需要存棋子的信息，没有棋子的位置都是一个值，那么我们何必将这些默认值也存到磁盘中呢？

稀疏数组一般有 n行3列
稀疏数组的第一行会存放
二维数组的行，二维数组的列，二维数组的有效值
其余行数存放
有效值的行，有效值的列，有效值

## 二维数组转稀疏数组
1. 遍历原始的二维数组，获取有有效值的个数sum
2. 然后形成 int[sum+1][3] 的稀疏数组
3. 二维数组的有效数据存入到 稀疏数组

## 稀疏数组还原成二维数组
1. 稀疏数组转 二维数组
2. 读取稀疏数组的第一行，然后创建二维数组
3. 读取稀疏数组后几行的数据，然后赋给原始数组

## 代码实现
```java
public class SparseArray {
    public static void main(String[] args) {
        //创建一个原始的二维数组 用来表示五指棋的棋盘
        //0 表示没有棋子 1 表示黑子 2 表示白子

        int[][] chessArr1 = new int[11][11];
        chessArr1[1][2] = 1;
        chessArr1[2][3] = 2;
        chessArr1[4][5] = 2;

        System.out.println("原始二维数组：");
        for (int[] row : chessArr1) {
            for (int col : row) {
                System.out.printf("%d\t",col);
            }
            System.out.println();
        }

        /*
        二维数组转 稀疏数组
        遍历原始的二维数组，获取有有效值的个数sum
        然后形成 int[sum+1][3] 的稀疏数组
        二维数组的有效数据存入到 稀疏数组
        */
        int sum = 0;
        for (int i = 0; i < 11; i++) {
            for(int j = 0; j < 11; j++) {
                if (chessArr1[i][j] != 0) {
                    sum++;
                }
            }
        }

        System.out.println("有效值一共有"+ sum +"个");

        //创建对应的 稀疏数组并初始化
        int[][] sparseArr = new int[sum+1][3];
        //第一列存放有 多少行，多少列，多少个有效值
        sparseArr[0][0] = 11;
        sparseArr[0][1] = 11;
        sparseArr[0][2] = sum;

        //然后将有效值的信息存入到其他行
        //比如第一个有效值是1 在 第二行的第三列，那么就设置成 1 2 1
        int row = 1;//记录第几个有效值
        for (int i = 0; i < 11; i++) {
            for(int j = 0; j < 11; j++) {
                if (row > sum) {
                    break;
                }
                if (chessArr1[i][j] != 0) {
                    sparseArr[row][0] = i;
                    sparseArr[row][1] = j;
                    sparseArr[row][2] = chessArr1[i][j];

                    row++;
                }
            }
        }
        
        System.out.println();
        System.out.println("压缩后的稀疏数组：");
        //输出稀疏数组
        for (int i = 0; i < sparseArr.length; i++) {
            System.out.printf("%d\t%d\t%d\t\n",sparseArr[i][0],sparseArr[i][1],sparseArr[i][2]);
        }

        /*
        稀疏数组转 二维数组

         读取稀疏数组的第一行，然后创建二维数组
         读取稀疏数组后几行的数据，然后赋给原始数组
        
         */
        int[][] chessArr2 = new int[sparseArr[0][0]][sparseArr[0][1]];

        for (int i = 1; i <= sparseArr[0][2]; i++) {
            chessArr2[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }

        System.out.println("还原后的二维数组：");
        for (int[] r : chessArr2) {
            for (int col : r) {
                System.out.printf("%d\t",col);
            }
            System.out.println();
            
        }
    }
}
```